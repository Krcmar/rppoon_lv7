﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON_LV7
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;

        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }

        public float GetPreviousCPULoad()
        {
            return previousCPULoad;
        }

        public float GetPreviousAvailableRAM()
        {
            return previousRAMAvailable;
        }

        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (Math.Abs(this.previousCPULoad - currentLoad) >= 0.1)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }

        public float GetAvailableRAM()
        {
            float currentAvailableRAM = this.previousRAMAvailable;
            if(Math.Abs(this.previousRAMAvailable - currentAvailableRAM) >= 0.1)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentAvailableRAM;
            return currentAvailableRAM;
        }
    }
}