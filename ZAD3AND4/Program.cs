﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider dataProvider = new SystemDataProvider();
            string filePath = @"C:\4. semestar\RPPOON\LV\RPPOON_LV7\RPPOON_LV7\logger.txt";
            FileLogger fileLogger = new FileLogger(filePath);
            ConsoleLogger consoleLogger = new ConsoleLogger();

            dataProvider.Attach(fileLogger);
            dataProvider.Attach(consoleLogger);

            while (true)
            {
                dataProvider.GetCPULoad();
                dataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }

            
        }
    }
}