﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 10, 5, 6, 8, 7, 2, 3, 4, 1 };
            NumberSequence numberSequence = new NumberSequence(array);
            numberSequence.InsertAt(5, -9);

            BubbleSort bubbleSort = new BubbleSort();
            SequentialSort sequentialSort = new SequentialSort();
            LinearSearch linearSearch = new LinearSearch();

            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSortStrategy(sequentialSort);
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSearchStrategy(linearSearch);
            Console.WriteLine(numberSequence.Search(5));
            Console.WriteLine(numberSequence.Search(100));
        }
    }
}