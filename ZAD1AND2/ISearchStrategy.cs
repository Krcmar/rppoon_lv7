﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON_LV7
{
    interface ISearchStrategy
    {
        int Search(double[] array, int number);
    }
}
