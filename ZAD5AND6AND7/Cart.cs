﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON_LV7
{
    class Cart
    {
        private List<IItem> items;
        private IVisitor visitor;

        public Cart(IVisitor visitor)
        {
            items = new List<IItem>();
            this.visitor = visitor;
        }

        public void SetVisitor(IVisitor visitor)
        {
            this.visitor = visitor;
        }

        public double Accept()
        {
            double sum = 0;
            foreach(IItem item in items)
            {
                sum += item.Accept(visitor);
            }
            return sum;
        }

        public void AddItem(IItem item)
        {
            items.Add(item);
        }

        public void RemoveItem(IItem item)
        {
            items.Remove(item);
        }
    }
}
