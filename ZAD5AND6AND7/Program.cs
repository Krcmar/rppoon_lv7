﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd = new DVD("bestselling movie", DVDType.SOFTWARE, 5.80);
            VHS vhs = new VHS("Godfather", 10.33);
            Book book = new Book("Patnje mladog Werthera", 2.53);

            BuyVisitor buyVisitor = new BuyVisitor();
            Console.WriteLine(buyVisitor.Visit(dvd));
            Console.WriteLine(buyVisitor.Visit(vhs));
            Console.WriteLine(buyVisitor.Visit(book));

            RentVisitor rentVisitor = new RentVisitor();
            Console.WriteLine(rentVisitor.Visit(dvd));
            Console.WriteLine(rentVisitor.Visit(vhs));
            Console.WriteLine(rentVisitor.Visit(book));

            Cart cart = new Cart(buyVisitor);
            cart.AddItem(dvd);
            cart.AddItem(vhs);
            cart.AddItem(book);

            Console.WriteLine(cart.Accept());
            cart.SetVisitor(rentVisitor);
            Console.WriteLine(cart.Accept());

        }
    }
}